require 'capybara'
include Capybara

Capybara.default_driver = :selenium
Capybara.default_wait_time = 10
Capybara.default_selector = :css
Capybara.app_host = 'http://sdl.webcoda.co.uk'

After do
  Capybara.reset_sessions!
end
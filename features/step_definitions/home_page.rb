When(/^I follow link in site$/) do
  visit '/'
  find('.jumbotron>p>a:first-child').click
end

Then(/^I should see download Bootstrap$/) do
  find('.btn.btn-outline-inverse.btn-lg')
end